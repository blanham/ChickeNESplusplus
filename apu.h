#ifndef APU_H
#define APU_H
#include <stdint.h>
class APU {
	public:
	APU();
	uint8_t read_apu(uint16_t addr);
	void write_apu(uint16_t addr, uint8_t value);
//	void run_apu(unsigned int cycles);
	uint32_t cycles_to_run;
};
#endif
