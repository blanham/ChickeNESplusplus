#include <boost/crc.hpp>//want to add CRC32 of rom for id purposes
#include <iostream>
#include <fstream>
#include <cstring>
#include "cartridge.h"
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

Cart::Cart(char *filename)
{
	ifstream romfile (filename, ios::in | ios::binary);
	
	if(!romfile.is_open())
		throw "File not found.";
	
	romfile.read((char *)&header, 16);
		
	if (memcmp(header.NES, "NES\x1A", 4) == 0)
        cout << "Detected iNES file: " << filename << endl;
    else
		throw "Not a valid iNES rom image!";
	
	prg = new uint8_t[header.PRG_banks * 0x4000];
	romfile.read((char *)prg, header.PRG_banks * 0x4000);
	
	//initialize prg_banks pointer array
	for(int i =0; i < header.PRG_banks*2; i++)
		prg_banks[i] = &prg[0x2000*i];
	
	//handles mirroring when prg is 0x4000 bytes long
	if(header.PRG_banks == 1)
	{
		prg_banks[2] = (uint8_t *)&prg[0];
		prg_banks[3] = (uint8_t *)&prg[0x2000];
	}
	if(header.CHR_banks != 0)
	{
		chr = new uint8_t[header.CHR_banks * 0x2000];
		romfile.read((char *)chr, header.CHR_banks * 0x2000);	
	}
	for(int i =0; i < header.CHR_banks*8; i++)
		chr_banks[i] = &chr[0x400*i];
		
	romfile.close();
	if(mapper())
	{
		print();
		throw "mappers not implemented yet";
	}	
	if(this->battery())
		cout << "Need to add save loading in Cart!" << endl;

	loaded = true;
}
Cart::~Cart()
{

	delete prg_banks;
	delete chr_banks;
	loaded = false;
}
void Cart::print(void)
{
	cout << "PRG size: " << hex << header.PRG_banks * 0x4000 << endl;
	cout << "CHR size: " << hex << header.CHR_banks * 0x2000 << endl;
	cout << "Region: "   << (region() ? "NTSC" : "PAL") << endl;
	cout << "Battery: "  << (battery() ? "Present" : "Not Present") << endl;
	cout << "Mapper: "   << (int)mapper() << endl;
}

