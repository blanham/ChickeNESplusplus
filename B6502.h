#ifndef B6502_H
#define B6502_H
#include <stdint.h>
#define rdRAM(addr) p_RAM[(addr) >> 12][(addr) & 0x0FFF]
#include <iostream>
using namespace std;
class B6502 {
	public:
		B6502();
		~B6502() {}
		void init_cpu();
		void do_op();
		void nmi();
		void irq();
		class EMULATOR *emulator;
		uint16_t get_pc() const {return PC;}
		bool is_running;
		//provided by external functions
		virtual uint8_t read_ram(uint16_t addr);
		virtual void write_ram(uint16_t addr, uint8_t value);
		uint8_t *p_RAM[16];				
		uint32_t get_cycles() const {return cycles;};
		bool NMI;
		bool IRQ;
		uint16_t PC;
	private:
		void logger(uint8_t op);
		uint8_t A, X, Y, P, S;	
		uint32_t cycles;
		uint32_t total_cycles;
		//UTILITY functions
		void push_stack();
		void printp();
		void chk_neg_zero(uint8_t chk);
		void branch(bool condition);
		//Memory
		uint8_t RD_ABS(); 
		uint8_t RD_ABS_X();
		uint8_t RD_ABS_Y();
		uint8_t RD_IND_X();
		uint8_t RD_IND_Y();
		uint8_t RD_IMM();	
		uint8_t RD_ZP(); 		
		uint8_t RD_ZP_X(); 		
		uint8_t RD_ZP_Y(); 		
		void 	WR_ABS  (uint8_t value); 
		void	WR_ABS_X(uint8_t value); 
		void 	WR_ABS_Y(uint8_t value); 
		void 	WR_IND_X(uint8_t value); 
		void 	WR_IND_Y(uint8_t value); 
		void 	WR_ZP   (uint8_t value);
		void 	WR_ZP_X (uint8_t value);
		void 	WR_ZP_Y (uint8_t value);
		void 	M_ABS   (uint8_t value); 
		void 	M_ABS_X (uint8_t value); 
		void 	M_ZP    (uint8_t value); 
		void 	M_ZP_X  (uint8_t value); 
		//Operations
		void 	ADC		(uint8_t val);
		uint8_t ASL		(uint8_t val); 
		void	BIT		(uint8_t val);
		void	CMP_R	(uint8_t reg, uint8_t val);
		uint8_t DEC		(uint8_t val);
		uint8_t INC		(uint8_t val);
		void 	JSR		();
		void 	JMP_IND	();
		uint8_t LSR		(uint8_t val);
		void 	PHA		();
		void	PHP		();
		void	PLA		();
		void	PLP		();
		uint8_t ROL		(uint8_t val);
		uint8_t ROR		(uint8_t val);
		void 	RTI		();
		void 	RTS		();
		void 	SBC		(uint8_t val);
};
  
inline void B6502::chk_neg_zero(uint8_t chk) 
{
    if (chk & 0x80) P |= 0x80;
    else P &= 0x7f;

    if (chk == 0) P |= 0x02;
    else P &= 0xfd;
}

inline void B6502::branch(bool condition)
{
	cycles = 2; 
    uint16_t savepc;
	if (condition) 
	{   	 
		savepc = PC + (int8_t)read_ram(PC+1);
	//	if ((PC>>8) != ((savepc)>>8))
      //      cycles+=1;

		PC = savepc;
		cycles+=1;
  	}	
	PC += 2;
}
#define ABSOLUTE read_ram(PC+1) + (read_ram(PC+2) << 8)
#define M_ABSOLUTE read_ram(PC-2) + (read_ram(PC-1) << 8)
#define IMMEDIATE read_ram(PC+1)
#define M_IMMEDIATE read_ram(PC-1)
inline uint8_t B6502::RD_ABS() 
{
	uint8_t val = read_ram(ABSOLUTE);
	PC += 3;
    cycles = 4;
    return val;
}

inline uint8_t B6502::RD_ABS_X() 
{
    uint16_t savepc = ABSOLUTE;
	PC += 3;
    cycles = 4;
    if ((savepc >>8) != ((savepc+X)>>8))
            cycles++;
    return read_ram(savepc + X);
}

inline uint8_t B6502::RD_ABS_Y() 
{
    uint16_t savepc = ABSOLUTE;
	PC += 3;
    cycles = 4;
    if ((savepc >>8) != ((savepc+Y)>>8))
            cycles++;
    return read_ram(savepc + Y);
}

inline uint8_t B6502::RD_IND_X()
{
    uint8_t val = read_ram(PC+1) + X;  
    uint16_t savepc = read_ram(val & 0xFF);//AND handles ZP wrap around
	savepc |= read_ram((val+1) & 0xFF) << 8;
	PC+=2;
    cycles = 6;

    return read_ram(savepc);
}

inline uint8_t B6502::RD_IND_Y()
{
	uint16_t val = read_ram(PC+1);
    uint16_t addr = read_ram(val);
	addr |= read_ram((val + 1) & 0xFF) << 8;
	cycles = 5;
	if ((addr >>8) != ((addr+Y)>>8))
		cycles++;
	PC+=2;
		
	val = read_ram(addr + Y);
	return val;
}

inline uint8_t B6502::RD_IMM() 
{
    PC += 2;
    cycles = 2;
	return read_ram(PC-1);
}
inline uint8_t B6502::RD_ZP() 
{
	PC+=2;
	cycles=3; 
	return read_ram(M_IMMEDIATE & 0xFF); 
}
inline uint8_t B6502::RD_ZP_X() 
{
	PC+=2;
	cycles=4; 
	return read_ram((M_IMMEDIATE + X) & 0xFF); 
}
inline uint8_t B6502::RD_ZP_Y() 
{
	PC+=2;
	cycles=4; 
	return read_ram((M_IMMEDIATE + Y) & 0xFF); 
}

/*==MEMORY WRITE FUNCTIONS==*/
inline void B6502::WR_ABS(uint8_t value) 
{
	write_ram(ABSOLUTE, value); 
	cycles=4; 
	PC+=3;
}
inline void B6502::WR_ABS_X(uint8_t value) 
{
	write_ram(ABSOLUTE + X, value); 
	cycles=5; 
	PC+=3;
}
inline void B6502::WR_ABS_Y(uint8_t value) 
{
	write_ram(ABSOLUTE + Y, value); 
	cycles=5; 
	PC+=3;
}
inline void B6502::WR_IND_X(uint8_t value) 
{
	uint8_t val = read_ram(PC+1) + X;  
    uint16_t addr = read_ram(val & 0xFF);//AND handles ZP wrap around
	addr |= read_ram((val+1) & 0xFF) << 8;
	write_ram(addr, value); 
	cycles=6; 
	PC+=2;
}

inline void B6502::WR_IND_Y(uint8_t value) 
{
   	uint16_t val = read_ram(PC+1);
    uint16_t addr = read_ram(val);
	addr |= read_ram((val+1) &0xFF) << 8;

	cycles=6;
	if ((addr >>8) != ((addr+Y)>>8))
		cycles++;
	write_ram(addr + Y, value); 
	PC+=2;
}
inline void B6502::WR_ZP(uint8_t value) 
{
   	write_ram(IMMEDIATE & 0xFF, value);
	cycles=3; 
	PC+=2;
}
inline void B6502::WR_ZP_X(uint8_t value) 
{
   	write_ram((IMMEDIATE + X) & 0xFF, value);
	cycles=4; 
	PC+=2;
}
inline void B6502::WR_ZP_Y(uint8_t value) 
{
   	write_ram((IMMEDIATE + Y) & 0xFF, value);
	cycles=4; 
	PC+=2;
}

inline void B6502::M_ABS(uint8_t value) 
{
	write_ram(M_ABSOLUTE, value); 
	cycles+=2; 
}
inline void B6502::M_ABS_X(uint8_t value) 
{
	write_ram(M_ABSOLUTE + X, value); 
	cycles+=3; 
}
inline void B6502::M_ZP(uint8_t value) 
{
   	write_ram(M_IMMEDIATE & 0xFF, value); 
	cycles+=2; 
}
inline void B6502::M_ZP_X(uint8_t value) 
{
   	write_ram((M_IMMEDIATE + X) & 0xFF, value);
	cycles+=2; 
}
inline void B6502::ADC(uint8_t val)
{
	//thanks to blargg and Wednesday for this implementation
	int16_t sum = (int8_t) A + (int8_t) val + (P & 0x1);
	
	//Overflow	
//	if ((A ^ sum) & (A ^ val) & 0x80) 
//		P |= 0x40;
  //  else 
	//	P &= 0xbf;
    if ((sum + 128) & 0x100) P |= 0x40;
    else P &= 0xbf;
	sum = A + (P& 0x1) + val;
	//Carry
	if (sum >> 8) 
		P |= 0x01; 
	else 
		P &= 0xfe;

	A  = sum & 0xFF;
	chk_neg_zero(A);
}
inline void B6502::SBC(uint8_t val)
{
	ADC(val ^ 0xFF);
}
inline uint8_t B6502::DEC(uint8_t val)
{
	val--;
	chk_neg_zero(val);
	return (int)val;
}
inline uint8_t B6502::INC(uint8_t val)
{
	val++;
	chk_neg_zero(val);
	return (int)val;
}
inline uint8_t B6502::ASL(uint8_t val) 
{
    P = (P & 0xfe) | ((val >>7) & 0x01);
    val = val << 1;
    chk_neg_zero(val);
	return val;
}
inline void B6502::BIT(uint8_t val) 
{
    if (val & A) P &= 0xfd;
    else P |= 0x02;
    P = (P & 0x3f) | (val & 0xc0);
}
inline void B6502::CMP_R(uint8_t reg, uint8_t val) 
{
    if (reg == val) P |= 0x02;
    else P &= 0xfd;
    
	if (reg >= val) P |= 0x01;
    else P &= 0xfe;

    if ((reg - (signed char)val) & 0x80) P |= 0x80;
    else P &= 0x7f;
}
inline void B6502::JMP_IND()
{
	uint16_t addr   = ABSOLUTE;
	uint16_t addr2  = read_ram(addr);
    if(IMMEDIATE ==0xFF){
		addr &= 0xFF00;
		addr -= 1;
    }
   	addr2 |= (read_ram(addr+1) << 8);
	PC = addr2; 
	cycles = 5;
}
inline void B6502::JSR() 
{
	uint16_t savepc = read_ram(PC+1) + (read_ram(PC + 2) << 8);

	PC += 2;
    p_RAM[0][0x0100+S--] = (PC  >>  8);
    p_RAM[0][0x0100+S--] = (PC & 0xff);

    PC = savepc;
    cycles = 6;
}
inline uint8_t B6502::LSR(uint8_t val)
{
	P = (P & 0xfe) | (val & 0x01);
	val = val >> 1;		
	chk_neg_zero(val);
	return val;
}
inline void B6502::PHA() 
{
    p_RAM[0][0x0100+S--] = A;
    cycles = 3;
	PC++;
}
inline void B6502::PHP() 
{
    uint8_t val=P;
    val |= 0x30;
    p_RAM[0][0x0100+S--]=val;
    cycles = 3;
	PC++;
}
inline void B6502::PLA() 
{
    A = p_RAM[0][0x0100 + ++S];
    chk_neg_zero(A);
    cycles = 4;
	PC++;
}
inline void B6502::PLP() 
{
    P = p_RAM[0][0x0100 + ++S] & 0xEF;
    P |= 0x20;
	cycles = 4;
	PC++;
}
inline uint8_t B6502::ROL(uint8_t val)
{    
	uint8_t carry_old = (P & 0x01);
    P = (P & 0xfe) | ((val >>7) & 0x01);
    val = val << 1;
    val |= carry_old;
    chk_neg_zero(val);
	return val;
}
inline uint8_t B6502::ROR(uint8_t val)
{    
	bool carry_old = (P & 0x01);
    P = (P & 0xfe) | (val & 0x01);
    val = val >> 1;
    if (carry_old) val |= 0x80;
    chk_neg_zero(val);
	return val;
}
inline void B6502::RTI() 
{
    P   = p_RAM[0][0x100 + ++S] | 0x20;
    PC  = p_RAM[0][0x100 + ++S];
    PC |= p_RAM[0][0x100 + ++S] << 8;
	cycles = 6;
}

inline void B6502::RTS() 
{
    PC  = p_RAM[0][0x100 + ++S];
	PC |= p_RAM[0][0x100 + ++S] << 8;
	PC++;
    cycles = 6;
}
#endif
