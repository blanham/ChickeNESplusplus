#include "ppu.h"
#include "nes.h"
#include <iostream>
#include <fstream>
//#include "SOIL.h"
#include <SDL.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif




using namespace std;


void dummy_render(uint8_t *dummy)
{
	cout << "Dummy renderer" << endl;
	return; 
}

PPU::PPU()
{
	ppu_status |= 0x10;//= 0x01;
	ppu_state = false;//latch for $2006/$2007
	ppu_render_function = &dummy_render;
    ppu_address = 0;
	frames = 0;
	first_read = true;
	scanline = -1;
	end_cycle = 341;
	cycle = 0;
	cycles_to_run = 0;
    is_rendering = false;
    on_screen = false;
	shortline = false;
	memset(&VRAM, 0xFF, 0x4000);//change to dynamic
	memset(&pixels, 0xFF, 256*240*4);//probably allocate this dynamically too
	//pixels = new uint8_t[256*240*4](); //new type[](); will initialize memory in accordance to C++03 standard
    x = 0;
	y = 0;
	loopy_x = 0;
	loopy_t = 0;
	loopy_v = 0;
}
/***Thanks to Disch for the idea behind this***/
void PPU::chr_pre_render()
{
	unsigned char chr_pre_render_buffer2[2][0x100][8][8]; 
	int buf_index;
	unsigned char lookup_table[4] = {0x00, 0xFD, 0xFE, 0xFF};
//	memset(chr_pre_render_buffer2, 0x34, 0x100*8*8*3);	
	
	for(int pat_table = 0; pat_table < 2; pat_table++)
	for(int tile = 0; tile < 0x100; tile++)
	{
		for(int x = 0; x < 8; x++)
		{
			for(int y = 0; y <8; y++)
			{
				chr_pre_render_buffer[pat_table][tile][x][y^7] = (VRAM[0x1000*pat_table + 0x10*tile + x] >> y) & 0x1;
				chr_pre_render_buffer[pat_table][tile][x][y^7] += ((VRAM[0x1000*pat_table+0x10*tile + 0x8 + x] >> y) & 0x1) *2;
	 			//use as index for bmp writing
				buf_index = (int)chr_pre_render_buffer[pat_table][tile][x][y^7];
				//set to values in lookup table
				chr_pre_render_buffer[pat_table][tile][x][y^7] = 16 * buf_index;//lookup_table[chr_pre_render_buffer[pat_table][tile][x][y^7]];
				//
		//		chr_pre_render_buffer2[pat_table][tile][x][y^7]= 64 * buf_index;
			}		
		}
	}
	
/*	int save_result = SOIL_save_image
	(
		"chr.bmp",
		SOIL_SAVE_TYPE_BMP,
		8, 2*8*0x100, 1,
		(unsigned char *)&chr_pre_render_buffer2
		);
	cout << "Bit map written"<< save_result << endl;*/	
}


uint8_t PPU::read_vram(uint16_t addr)
{
	return VRAM[addr];

}
void PPU::write_vram(uint16_t addr, uint8_t val)
{
	VRAM[addr] = val;
}
uint8_t PPU::read_ppu(uint16_t addr)
{
//	cout << "PPU read @ Address: " << hex << (int)addr << endl;
	uint8_t tmp;
	switch(addr)
	{
		case 0x2002:
			ppu_state = false;
			tmp =  ppu_status;
			ppu_status &= ~(0x80|0x40);
			return tmp;
		case 0x2007: 
			if (ppu_ctrl & 0x4)
			{	
				ppu_address += 32;
				loopy_v +=32;
			}
			else 
			{
				ppu_address += 1;
				loopy_v++;
			}
			if (first_read)	
			{
				//return (first_read = false); 
				first_read = false;
				return 0;
			}
			else 
				return read_vram(ppu_address);
		case 0x2004: 
			return OAM[oam_address];
	}

	cout << "Invalid address passed to read_ppu!" <<  endl;
	abort();
}

void PPU::write_ppu(uint16_t addr, uint8_t value)
{

//	cout << "PPU write @ Address: " << (int)addr << " Value: " << (int)value << endl;
	switch(addr)
	{
		case 0x2000: 
			ppu_ctrl = value; 
		    loopy_t &= 0x73FF;
        	loopy_t |= (value & 0x03) << 10;
		break;	
		case 0x2001: 
			ppu_mask = value; 
		break;
		case 0x2003: 
			oam_address = value; 
		break;
		case 0x2004:
			OAM[oam_address++] = value; 
		break;
		case 0x2005:
			#warning "Scroll not correctly implmented!"
			if(ppu_state)
			{	
				loopy_x = (value & 0x7) ;
            	loopy_t &= 0x7FE0;
            	loopy_t |= ((value&0xF8)>>3);
				ppu_scroll = (ppu_scroll & 0x3F00) | value; 
			}
			else
			{
				loopy_t &= 0xC1F;
				loopy_t |= ((value & 0xF8)<<2) | ((value & 0x07)<<12);
				ppu_scroll = (ppu_scroll & 0xFF) | (value << 8);
			}
			ppu_state = !ppu_state;		
		break;
		case 0x2006:
			if(ppu_state)
			{	
			
				loopy_t &= 0x00FF;
            	loopy_t |= (value & 0x3F) << 8;
					ppu_address = (ppu_address & 0x3F00) | value; 
			}
			else
			{
				loopy_t &= 0x7F00;
			   	loopy_t |= value;
				loopy_v = loopy_t;

				ppu_address = (ppu_address & 0xFF) | (value << 8);
			}
			ppu_state = !ppu_state;
		break;
		case 0x2007: 
			write_vram(ppu_address, value);  
			if (ppu_ctrl & 0x4)
			{
				loopy_v += 32;
          		ppu_address += 32;
			}
        	else
			{
				loopy_v++;	
				ppu_address += 1;
			}
		break;
		default:
		throw "Invalid address passed to read_ppu!";
		break;

	}
	//Run ppu to catch up
	
	run_ppu();
	

}

void PPU::dump_vram()
{
	ofstream vram_dump("vram.dmp", ios::out | ios::binary);
	if(!vram_dump.is_open()) abort();
	vram_dump.write((char *)&VRAM, 0x4000);
	vram_dump.close();
}

void PPU::run_ppu()
{
	int tile = 0;
	int pixel_index = 0;
	while(cycles_to_run)
	{
		
//		cout << "PPU cycle: " << cycle << endl;	
		if (cycle == 256)
		{
				//fake sprite 0
				if (scanline==30) ppu_status |=0x40;
				//	if (scanline > -1)
						end_cycle = 341;
				//	else if (shortline && is_rendering)
				//		end_cycle = 340;
		}
		else if (cycle == 304)
		{
		 	if ((scanline < 0) && is_rendering)
			{
				loopy_v = loopy_t;
				first_read = true;
			}
		}
		else if (cycle == end_cycle)
		{
		
			scanline++;
			cycle = 0;

			if (scanline == 0)
				on_screen = true;
			else if (scanline == 240){
				on_screen = is_rendering = false;
			}
			else if (scanline == 241)
			{
				
				ppu_render_function((uint8_t *)&pixels);
					

				x=0;

				//wait (slows down)
				//	usleep(5000);
				frames++;
						
				//we are in vblank!
			ppu_status |= 0x80;
				//sprcounter reset
		//	RAM[0x2003] = 0;
			//Set NMI
			if (ppu_ctrl & 0x80)
				((class NES *)nes)->cpu->NMI = true;
			}
		}
		else if (scanline == 341)
		{
			scanline = -1;
			if(ppu_mask & 0x18) 
			is_rendering = true;

	//		if (!region)
	//			shortline = !shortline;
	//		else
	//		shortline = false;
		}
		else if ((scanline < 0) && (cycle == 1))
		{
			ppu_status = 0;
		}

		//tile = 0;
		if(is_rendering)
		{
			/*

				prebuffer[
			*/
			/*
			Rendering code by me
			nametable addressing code thanks to qeed(nes_emu.txt)

			*/	   
			switch(cycle)
			{
  				
				case   0:	case   8: 	case  16: 	case  24: 
				case  32: 	case  40: 	case  48: 	case  56:
				case  64: 	case  72:   case  80:   case  88:
				case  96:   case 104:   case 112:   case 120:
				case 128:   case 136:   case 144:   case 152:
				case 160:   case 168:   case 176:   case 184:
				case 192:   case 200:   case 208:   case 216:
				case 224:   case 232:   case 240:   case 248:
				tile = VRAM[(loopy_v & 0x3FFF) + 0x2000 + 0x40];
				
				cout << "PPU addres:" << hex << (int)loopy_v <<"\tTile: " << hex << (int)tile << endl;	
				break;
				
				case   1:	case   9:   case  17:   case  25:
				case  33:	case  41:

				break;
				case   3:	case  11:	case  19:	case  27:
				case  35:	case  43:	case  51:	case  59:
				case  67:   case  75:   case  83:   case  91:
				case  99:   case 107:   case 115:   case 123:
				case 131:   case 139:   case 147:   case 155:
				case 163:   case 171:   case 179:   case 187:
				case 195:   case 203:   case 211:   case 219:
				case 227:   case 235:   case 243:	
				if((loopy_v & 0x1F) == 0x1F)
					loopy_v ^= 0x41F;
				else
					loopy_v++;
				break;
				case  65:   case  73:   case  81:   case  89:
				case  97:   case 105:   case 113:   case 121:
				case 129:   case 137:   case 145:   case 153:
				case 161:   case 169:   case 177:   case 185:
				case 193:   case 201:   case 209:   case 217:
				case 225:   case 233:   case 241:   case 249:

			//	ppu_address+=8;	
				
				break;
				case 256:   case 264:   case 272:   case 280:
				case 288:   case 296:   case 304:   case 312:
					loopy_v = 0x2000 | (ppu_address & 0xFFF);
					break;
				case 251:
					if ((loopy_v & 0x1F) == 0x1F)
						loopy_v ^= 0x41F;
					else
						loopy_v++;

					// This is the same as above when applying the
					//   attribute data 

					if ((loopy_v & 0x7000) == 0x7000)
					{
						int tmp = loopy_v & 0x3E0;
						//reset tile y offset 12 - 14 in addr
						loopy_v &= 0xFFF;
						switch (tmp)
						{
						//29, flip bit 11
							case 0x3A0:
								loopy_v ^= 0xBA0;
							break;
							case 0x3E0: //31, back to 0
								loopy_v ^= 0x3E0;
							break;
							default: //inc y scroll if not reached
								loopy_v += 0x20;
						}
					}
					else //inc fine y
						loopy_v += 0x1000;
				break;
				case 332:
				break;
				case 257:
					loopy_v &= 0xFBE0;
					loopy_v |= (loopy_t & 0x41F);
					//rndraddr = 0x2000 | (V & 0xFFF);
				break;
				case 323:    case 331:

					if ((loopy_v & 0x1F) == 0x1F)
						loopy_v ^= 0x41F;
					else
						loopy_v++;
				break;
				
				default:
				break;
			}	
						
				
		/*	if(on_screen && cycle == 256)
			{
				for(int l =0; l < 8; l++)	
				{
					for(int j = 0; j < 32; j++)
					for(int k =0; k < 8; k++)	
					{
						tile = VRAM[0x2000 + 0x40+ j];
						pixel_index = chr_pre_render_buffer[1][tile][k][scanline % 8];
								pixels[l][j*32 + k][0] =  nes_pal[pixel_index][0];//nes_pal[PAL[pindex]][0];
								pixels[l][j*32 +k][1] =  nes_pal[pixel_index][1];//nes_pal[PAL[pindex]][0];
								pixels[l][k][2] =  nes_pal[pixel_index][2];//nes_pal[PAL[pindex]][0];


					}
				}


			}*/
			if(on_screen && cycle < 256)
			{	
								
				pixel_index = chr_pre_render_buffer[1][tile][scanline %8][cycle%8];
								pixels[scanline][cycle][0] =  nes_pal[pixel_index*4][0];//nes_pal[PAL[pindex]][0];
								pixels[scanline][cycle][1] =  nes_pal[pixel_index*4][1];//nes_pal[PAL[pindex]][0];
								pixels[scanline][cycle][2] =  nes_pal[pixel_index*4][2];//nes_pal[PAL[pindex]][0];

						
			}


		}






		cycles_to_run--;
		cycle++;
	}

}


GLbyte PPU::nes_pal[64][4] = {
	{0x75, 0x75, 0x75, 0},
	{0x21, 0x1b, 0x8f, 0},
	{0x00, 0x00, 0xab, 0},
	{0x47, 0x00, 0x9f, 0},
	{0x8f, 0x00, 0x77, 0},
	{0xab, 0x00, 0x13, 0},
	{0xa7, 0x00, 0x00, 0},
	{0x7f, 0x0b, 0x00, 0},
	{0x43, 0x2f, 0x00, 0},
	{0x00, 0x47, 0x00, 0},
	{0x00, 0x51, 0x00, 0},
	{0x00, 0x3f, 0x17, 0},
	{0x1b, 0x3f, 0x5f, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0xbc, 0xbc, 0xbc, 0},
	{0x00, 0x73, 0xef, 0},
	{0x23, 0x3b, 0xef, 0},
	{0x83, 0x00, 0xf3, 0},
	{0xbf, 0x00, 0xbf, 0},
	{0xe7, 0x00, 0x5b, 0},
	{0xdb, 0x2b, 0x00, 0},
	{0xcb, 0x4f, 0x0f, 0},
	{0x8b, 0x73, 0x00, 0},
	{0x00, 0x97, 0x00, 0},
	{0x00, 0xab, 0x00, 0},
	{0x00, 0x93, 0x3b, 0},
	{0x00, 0x83, 0x8b, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0xff, 0xff, 0xff, 0},
	{0x3f, 0xbf, 0xff, 0},
	{0x5f, 0x97, 0xff, 0},
	{0xa7, 0x8b, 0xfd, 0},
	{0xf7, 0x7b, 0xff, 0},
	{0xff, 0x77, 0xb7, 0},
	{0xff, 0x77, 0x63, 0},
	{0xff, 0x9b, 0x3b, 0},
	{0xf3, 0xbf, 0x3f, 0},
	{0x83, 0xd3, 0x13, 0},
	{0x4f, 0xdf, 0x4b, 0},
	{0x58, 0xf8, 0x98, 0},
	{0x00, 0xeb, 0xdb, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0xff, 0xff, 0xff, 0},
	{0xab, 0xe7, 0xff, 0},
	{0xc7, 0xd7, 0xff, 0},
	{0xd7, 0xcb, 0xff, 0},
	{0xff, 0xc7, 0xff, 0},
	{0xff, 0xc7, 0xdb, 0},
	{0xff, 0xbf, 0x83, 0},
	{0xff, 0xdb, 0xab, 0},
	{0xff, 0xe7, 0xa3, 0},
	{0xe3, 0xff, 0xa3, 0},
	{0xab, 0xf3, 0xbf, 0},
	{0xb3, 0xff, 0xcf, 0},
	{0x9f, 0xff, 0xf3, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0},
	{0x00, 0x00, 0x00, 0}
};

