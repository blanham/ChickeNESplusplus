#include <iostream>
#include <SDL/SDL.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include "wx/wxprec.h"
 
#ifndef WX_PRECOMP
#	include "wx/wx.h"
#endif

#include "nes.h"
#include <signal.h>
#include <unistd.h>

using namespace std;
NES *fuck;
GLuint texture;			// This is a handle to our texture object
SDL_Surface *screen;
class BNES_APP {
	NES* nes;	

};
void handler (int signal)
{

	fuck->ppu->dump_vram();
	fuck->dump_ram();	
	exit(0);
}

int init_sdl()
{	
    // initialize SDL video
    if ( SDL_Init( SDL_INIT_VIDEO| SDL_INIT_JOYSTICK | SDL_INIT_AUDIO) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }
    SDL_WM_SetCaption("BNES++","BNES++");
    // make sure SDL cleans up before exit
    atexit(SDL_Quit);
    
	//enables OpenGL double buffering
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // create a new window
    //fullscreen fix at
    //http://lists.libsdl.org/pipermail/sdl-libsdl.org/2002-November/032031.html
  
//	mainwindow = SDL_CreateWindow("test",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,512,480, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  	screen = SDL_SetVideoMode(512, 480, 8, SDL_OPENGL| SDL_HWSURFACE);
 	//if (!mainwindow) /* Die if creation failed */
      //  exit(1);

//	screen2 = SDL_GL_CreateContext(mainwindow);
    SDL_GL_SetSwapInterval(1);
    if (!screen )
    {
        printf("Unable to set 256x240 video: %s\n", SDL_GetError());
        return 1;
    }
	
	//OpenGL Setup
	glEnable(GL_TEXTURE_2D);
	
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0,512, 480);
	
	glClear(GL_COLOR_BUFFER_BIT);
//	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	glOrtho(0.0f, 256, 240, 0.0f, -1.0f, 1.0f);
	
	glMatrixMode(GL_MODELVIEW);
	/**init texture shit**/
	glGenTextures( 1, &texture );
 
	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, texture );
	
 	glTexImage2D(GL_TEXTURE_2D, 0, 4, 256, 240, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    return 0;
}


void render_function(uint8_t *pixels)
{
	glClear(GL_COLOR_BUFFER_BIT);
	//glGenTextures( 1, &texture );
 
	// Bind the texture object
	glBindTexture(GL_TEXTURE_2D, texture );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 240, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glBegin(GL_QUADS);
		glTexCoord2d(0,0); glVertex2i(0, 0);
		glTexCoord2d(1,0); glVertex2i(256, 0);
		glTexCoord2d(1,1); glVertex2i(256, 240);
		glTexCoord2d(0,1); glVertex2i(0, 240);
	glEnd();
	SDL_GL_SwapBuffers();
//	SDL_GL_SwapWindow(mainwindow);
}
int main(int argc, char **argv)
{
	signal(SIGINT,handler);
	NES *nes = NULL;
	init_sdl();
	cout << "NES++ v0.01" <<endl;
	try {
		nes = new NES(argv[1]);
	}
	catch (char *e){
		cout << e << endl;
		exit(-1);
	}
	fuck = nes;	
	if(argc == 3)
	{
	//	nes->stop_pc = (uint16_t)strtoul(argv[2], NULL, 16); 
		nes->ppu->frames_to_run = atoi(argv[2]);// = (uint16_t)strtoul(argv[2], NULL, 16); 
	}else {
		cout << "Invalid number of arguments" << endl;
		exit(-1);
	}
	nes->ppu->ppu_render_function = &render_function;	
	nes->cart->print();
	nes->run_nes();
	

	fuck->ppu->dump_vram();
	fuck->dump_ram();
	delete nes;
	return 0;
}
