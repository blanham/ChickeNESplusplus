#ifndef PPU_H
#define PPU_H
#include <stdint.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
class PPU {
	public:
	PPU();
//	~PPU(); //PPU should have destructor
	void 		run_ppu();
	uint8_t		read_ppu(uint16_t addr);
	void 		write_ppu(uint16_t addr, uint8_t value);
	void 		dump_vram();
	//pointer to parent class
	class NES* 	nes;
	//cycles to run to catch up to cpu
	uint32_t 	cycles_to_run;
	uint8_t 	VRAM[0x4000];
	uint8_t 	ppu_ctrl;
	uint8_t 	pixels[240][256][4];
	uint32_t 	frames;
	//temporary for testing	
	uint32_t 	frames_to_run;	
	void 		chr_pre_render();
	//pointer to external render function
	//might want to have contructor set it
	void		(*ppu_render_function)(uint8_t *);
	private:
	uint8_t 	chr_pre_render_buffer[2][0x200][8][8];
	uint8_t 	OAM[0xFF];
	uint8_t 	palette[0x20];
	uint8_t 	read_vram(uint16_t addr);
	void 		write_vram(uint16_t addr, uint8_t val);
	enum 		t_mirror {MIRROR_HORIZONTAL, MIRROR_VERTICAL, MIRROR_FOUR_SCREEN} ;
	t_mirror 	mirroring;
	uint16_t	loopy_t;
	uint8_t		loopy_x;
	uint8_t		loopy_v;
	//registers and latch
	bool 		first_read;
	uint8_t 	ppu_mask;
	uint8_t 	ppu_status;
	uint16_t 	ppu_scroll;
	bool 		ppu_state;
	uint8_t 	oam_address;
	uint16_t 	ppu_address;
	//rendering status
	uint32_t 	cycle;
	int16_t 	scanline;
	uint16_t 	end_cycle;
	bool 		is_rendering;
	bool 		on_screen;
	bool 		shortline;
	static 		GLbyte nes_pal[64][4];
int x;
int y;
};

#endif
