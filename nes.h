#include <iostream>
#include "cartridge.h"
#include "B6502.h"
#include "ppu.h"
#include "apu.h"
#include "input.h"
//Macros for accessing RAM
#define rdCHRRAM(addr) p_VRAM [(addr) >> 11] [(addr) & 0x07FF]
#define rdNT(addr) p_NT [(addr & 0xC00) >> 10] [(addr) & 0x03FF]


class NES: public B6502, public PPU {
	public:
		NES(char *rom);
		~NES() {}
		void run_nes();	
		Cart *cart;
		B6502 *cpu;	
		PPU *ppu;
		APU *apu;
		INPUT *input;
		uint8_t RAM[0x800];//main NES ram	
		uint16_t stop_pc;
void dump_ram();
};
