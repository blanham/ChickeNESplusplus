#include "B6502.h"
#include <iostream>
#include <stdio.h>
using namespace std;

B6502::B6502()
{
	A=X=Y=0; //set arithmetical registers to zero
	S=0xFD; //set stack pointer to 0xFF
	PC = 0;
	P = 0x24; //set 
	NMI = false;
	IRQ = false;
	is_running = false;
	total_cycles = 0;
}

void B6502::init_cpu()
{
	PC = read_ram(0xFFFC) + (read_ram(0xFFFD) << 8);
//	PC = 0xC000;
	cout << "Starting execution at:" << hex <<  PC << endl;
}
void B6502::printp()
{
    printf("P:");
    if (P & 0x80)printf("N");
    else printf("n");
    if (P & 0x40)printf("V");
    else printf("v");
    if (P & 0x20)printf("U");
    else printf("u");
    if (P & 0x10)printf("B");
    else printf("b");
    if (P & 0x08)printf("D");
    else printf("d");
    if (P & 0x04)printf("I");
    else printf("i");
    if (P & 0x02)printf("Z");
    else printf("z");
    if (P & 0x01)printf("C");
    else printf("c");
}
void B6502::do_op()
{
	uint8_t op = read_ram(PC);	
	//printf("%.4X  %.2X  ",PC, op);
	//printf("A:%.2X X:%.2X Y:%.2X P:%.2X SP:%.2X CYC:%3i\n", A, X, Y, P, S, total_cycles);
	//logger(op);
	switch(op)
	 {
			//Break
		case 0x0: 
		cout << "BRK!" << endl;
		cout << "Halting CPU" << endl;
		is_running = false;
		throw 0;
		break;
			//ADC
		case 0x61:/*IND X*/  ADC(RD_IND_X()); break;
		case 0x65:/*ZP*/  ADC(RD_ZP()); break;
		case 0x69:/*IMM*/ ADC(RD_IMM()); break;
		case 0x6D:/*ABS*/ ADC(RD_ABS()); break;
		case 0x71:/*IND Y*/  ADC(RD_IND_Y()); break;
		case 0x75:/*ZP X*/  ADC(RD_ZP_X()); break;
		case 0x79:/*ABS Y*/ ADC(RD_ABS_Y()); break;
		case 0x7D:/*ABS X*/ ADC(RD_ABS_X()); break;
			//AND
		case 0x21:/*IND X*/	   A &= RD_IND_X();   chk_neg_zero(A); break;
		case 0x31:/*IND Y*/	   A &= RD_IND_Y();   chk_neg_zero(A); break;
		case 0x25:/*ZP*/	   A &= RD_ZP();  chk_neg_zero(A); break;
		case 0x29:/*IMM*/	   A &= RD_IMM();  chk_neg_zero(A); break;
		case 0x2D:/*ABS*/      A &= RD_ABS();  chk_neg_zero(A);	break;	
		case 0x35:/*ZP X*/	   A &= RD_ZP_X();  chk_neg_zero(A); break;
		case 0x39:/*ABS Y*/    A &= RD_ABS_Y();chk_neg_zero(A);	break;	
		case 0x3D:/*ABS X*/    A &= RD_ABS_X();chk_neg_zero(A);	break;	
			//ASL
		case 0x06:/*ZP*/	   M_ZP(ASL(RD_ZP())); break;
		case 0x0A:/*ACC*/	   A = ASL(A); cycles=2; PC++; break;
		case 0x0E:/*ABS*/	   M_ABS(ASL(RD_ABS())); break;
		case 0x16:/*ZP X*/	   M_ZP_X(ASL(RD_ZP_X())); break;
		case 0x1E:/*ABS X*/	   M_ABS_X(ASL(RD_ABS_X())); break;
			//Branches
		case 0x10:/*BPL*/ branch(!(P & 0x80)); 	break;		
		case 0x30:/*BMI*/ branch(P & 0x80); 	break;	
		case 0x50:/*BVC*/ branch(!(P & 0x40)); 	break;		
		case 0x70:/*BVS*/ branch(P & 0x40); 	break;	
		case 0x90:/*BCC*/ branch(!(P & 0x1));	break;
		case 0xB0:/*BCS*/ branch(P & 0x1);		break;
		case 0xD0:/*BNE*/ branch(!(P & 0x2));	break;
		case 0xF0:/*BEQ*/ branch(P & 0x2);		break;
			//BIT
		case 0x24:/*ZP */BIT(RD_ZP()); break; 
		case 0x2C:/*ABSOLUTE*/BIT(RD_ABS()); break; 
			//CMP
		case 0xC1:/*IND X*/ CMP_R(A,RD_IND_X()); break;
		case 0xC5:/*ZP*/ CMP_R(A,RD_ZP()); break;
		case 0xC9:/*IMM*/ CMP_R(A,RD_IMM()); break;
		case 0xCD:/*ABS*/ CMP_R(A,RD_ABS()); break;
		case 0xD1:/*IND Y*/ CMP_R(A,RD_IND_Y()); break;
		case 0xD5:/*ZP X*/ CMP_R(A,RD_ZP_X()); break;
		case 0xD9:/*ABS Y*/ CMP_R(A,RD_ABS_Y()); break;
		case 0xDD:/*ABS X*/ CMP_R(A,RD_ABS_X()); break;
			//CPX
		case 0xE0:/*IMM*/ CMP_R(X,RD_IMM()); break;
		case 0xE4:/*ZP*/ CMP_R(X,RD_ZP()); break;
		case 0xEC:/*ABS*/ CMP_R(X,RD_ABS()); break;
			//CPY
		case 0xC0:/*IMM*/ CMP_R(Y,RD_IMM()); break;
		case 0xC4:/*ZP*/ CMP_R(Y,RD_ZP()); break;
		case 0xCC:/*ABS*/ CMP_R(Y,RD_ABS()); break;
			//DEC
		case 0xC6:/*ZP*/ M_ZP(DEC(RD_ZP())); break;
		case 0xCE:/*ABS*/ M_ABS(DEC(RD_ABS())); 	break;
		case 0xD6:/*ZP X*/ M_ZP_X(DEC(RD_ZP_X())); break;
		case 0xDE:/*ABS X*/ M_ABS_X(DEC(RD_ABS_X())); 	 break;
			//DEX
		case 0xCA: X--;	chk_neg_zero(X);	cycles=2;	PC++;	break;
			//DEY
		case 0x88: Y--;	chk_neg_zero(Y);	cycles=2;	PC++;	break;		
			//EOR
		case 0x41:/*IND X*/	   A ^= RD_IND_X();	chk_neg_zero(A); 	break;
		case 0x45:/*ZP*/	   A ^= RD_ZP();	chk_neg_zero(A); 	break;
		case 0x49:/*IMM*/	   A ^= RD_IMM();	chk_neg_zero(A); 	break;
		case 0x4D:/*ABS*/	   A ^= RD_ABS();	chk_neg_zero(A); 	break;
		case 0x51:/*IND Y*/	   A ^= RD_IND_Y();	chk_neg_zero(A); 	break;
		case 0x55:/*ZP X*/	   A ^= RD_ZP_X();	chk_neg_zero(A); 	break;
		case 0x59:/*ABS Y*/    A ^= RD_ABS_Y();	chk_neg_zero(A);	break;	
		case 0x5D:/*ABS X*/    A ^= RD_ABS_X();	chk_neg_zero(A);	break;	
			//Flags
        case 0x18: 			  P &= 0xFE;  cycles = 2;	PC++;	break;
        case 0x38: 			  P |= 0x01;  cycles = 2;	PC++;	break;
        case 0x58: 			  P &= 0xFB;  cycles = 2;	PC++;	break;
        case 0x78: 			  P |= 0x04;  cycles = 2;	PC++;	break;
        case 0xB8:			  P &= 0xBF;  cycles = 2;	PC++;	break;
        case 0xD8: 			  P &= 0xF7;  cycles = 2;	PC++;	break;
        case 0xF8: 			  P |= 0x08;  cycles = 2;	PC++;	break;
			//INC
		case 0xE6:/*ZP*/	  M_ZP(INC(RD_ZP()));  break;
		case 0xF6:/*ZP X*/	  M_ZP_X(INC(RD_ZP_X()));  break;
		case 0xEE:/*ABS*/     M_ABS(INC(RD_ABS()));  break;
		case 0xFE:/*ABS X*/     M_ABS_X(INC(RD_ABS_X()));  break;
			//INY
		case 0xC8:             Y++; chk_neg_zero(Y);cycles=2;PC++;break;
			//INX
		case 0xE8:             X++; chk_neg_zero(X);cycles=2;PC++;break;
			//Jumps
		case 0x20:/*JSR*/      JSR(); break;
		case 0x40:/*RTI*/      RTI(); break;
		case 0x60:/*RTS*/      RTS(); break;
		case 0x4C:/*JMP ABS*/  PC = ABSOLUTE;  cycles=3; 		break;
		case 0x6C:/*JMP IND*/  JMP_IND();						break;
			//LDA
		case 0xA5:/*ZP*/	   A = RD_ZP();	   chk_neg_zero(A); break;
		case 0xB5:/*ZP_X*/	   A = RD_ZP_X();  chk_neg_zero(A); break;
		case 0xA9:/*IMM*/	   A = RD_IMM();   chk_neg_zero(A); break;		
		case 0xAD:/*ABSOLUTE*/ A = RD_ABS();   chk_neg_zero(A); break;	
		case 0xB1:/*IND_Y*/    A = RD_IND_Y(); chk_neg_zero(A); break;
		case 0xA1:/*IND_X*/    A = RD_IND_X(); chk_neg_zero(A); break;
		case 0xB9:/*ABS_Y*/    A = RD_ABS_Y(); chk_neg_zero(A); break;
		case 0xBD:/*ABS_X*/    A = RD_ABS_X(); chk_neg_zero(A); break;
			//LDX
		case 0xA2:/*IMM*/ 	   X = RD_IMM();   chk_neg_zero(X); break;
		case 0xA6:/*ZP*/	   X = RD_ZP();	   chk_neg_zero(X); break;
		case 0xAE:/*ABSOLUTE*/ X = RD_ABS();   chk_neg_zero(X); break;
		case 0xB6:/*ZP Y*/	   X = RD_ZP_Y();	   chk_neg_zero(X); break;
		case 0xBE:/*ABS Y*/    X = RD_ABS_Y(); chk_neg_zero(X); break;
			//LDY
		case 0xA0:/*IMM*/ 	   Y = RD_IMM();   chk_neg_zero(Y); break; 
		case 0xA4:/*ZP*/	   Y = RD_ZP();	   chk_neg_zero(Y); break;
		case 0xAC:/*ABSOLUTE*/ Y = RD_ABS();   chk_neg_zero(Y); break;
		case 0xB4:/*ZP X*/	   Y = RD_ZP_X();	   chk_neg_zero(Y); break;
		case 0xBC:/*ABS X*/    Y = RD_ABS_X();   chk_neg_zero(Y); break;
			//LSR
		case 0x46:/*ZP*/	   M_ZP(LSR(RD_ZP())); 	break;
		case 0x4A:/*ACC*/	   A = LSR(A); 	   cycles=2; PC++; 	break;
		case 0x4E:/*ABS*/	   M_ABS(LSR(RD_ABS())); 	break;
		case 0x56:/*ZP X*/	   M_ZP_X(LSR(RD_ZP_X())); 	break;
		case 0x5E:/*ABS X*/	   M_ABS_X(LSR(RD_ABS_X())); 	break;
			//NOPs
		case 0xEA: PC++; cycles=2; break;
			//ORA
		case 0x05:/*ZP*/	   A |= RD_ZP();   chk_neg_zero(A); break;
		case 0x09:/*IMM*/	   A |= RD_IMM();  chk_neg_zero(A); break;
		case 0x0D:/*ABS*/	   A |= RD_ABS();  chk_neg_zero(A); break;
		case 0x01:/*IND_X*/    A |= RD_IND_X();chk_neg_zero(A); break;	
		case 0x11:/*IND_Y*/    A |= RD_IND_Y();chk_neg_zero(A); break;	
		case 0x15:/*ZP X*/	   A |= RD_ZP_X();   chk_neg_zero(A); break;
		case 0x19:/*ABS Y*/	   A |= RD_ABS_Y();  chk_neg_zero(A); break;
		case 0x1D:/*ABS X*/	   A |= RD_ABS_X();  chk_neg_zero(A); break;
			//Pushes and Pulls
		case 0x08:/*PHP*/      PHP(); break;
		case 0x28:/*PLP*/      PLP(); break;
		case 0x48:/*PHA*/      PHA(); break;
		case 0x68:/*PLA*/      PLA(); break;
			//ROL
		case 0x26:/*ZP*/	   M_ZP(ROL(RD_ZP()));     break;
		case 0x2A:/*ACC*/	   A = ROL(A);     cycles=2; PC++;	break;
		case 0x2E:/*ABS */	   M_ABS(ROL(RD_ABS()));		break;
		case 0x36:/*ZP X*/	   M_ZP_X(ROL(RD_ZP_X()));     break;
		case 0x3E:/*ABS X*/	   M_ABS_X(ROL(RD_ABS_X()));		break;
			//ROR
		case 0x66:/*ZP*/	   M_ZP(ROR(RD_ZP()));		break;
		case 0x6A:/*ACC*/	   A = ROR(A);     cycles=2; PC++;	break;
		case 0x6E:/*ABS */	   M_ABS(ROR(RD_ABS()));		break;
		case 0x76:/*ZP x*/	   M_ZP_X(ROR(RD_ZP_X()));		break;
		case 0x7E:/*ABS X*/	   M_ABS_X(ROR(RD_ABS_X()));		break;
			//SBC
		case 0xE1:/*IND X*/ SBC(RD_IND_X()); break;
		case 0xE5:/*ZP*/ SBC(RD_ZP()); break;
		case 0xED:/*ABS */	   SBC(RD_ABS());					break;	
		case 0xF1:/*IND Y*/ SBC(RD_IND_Y()); break;
		case 0xF5:/*ZP X*/ SBC(RD_ZP_X()); break;
		case 0xF9:/*ABS Y*/	   SBC(RD_ABS_Y());					break;	
		case 0xE9:/*IMM*/ SBC(RD_IMM()); break;
		case 0xFD:/*ABS X*/	   SBC(RD_ABS_X());					break;	
			//STA
		case 0x85:/*ZP*/       WR_ZP(A);   break;
		case 0x8D:/*ABSOLUTE*/ WR_ABS(A);  break;
		case 0x91:/*IND Y*/    WR_IND_Y(A);break;
		case 0x81:/*IND X*/    WR_IND_X(A);   break;
		case 0x95:/*ZP X*/     WR_ZP_X(A);   break;
		case 0x99:/*ABS Y*/    WR_ABS_Y(A);break;
		case 0x9D:/*ABS X*/    WR_ABS_X(A);break;
			//STX	
		case 0x86:/*ZP*/ 	   WR_ZP(X);   break;
		case 0x8E:/*ABS*/	   WR_ABS(X);  break;
		case 0x96:/*ZP Y*/ 	   WR_ZP_Y(X);   break;
			//STY
		case 0x84:/*ZP*/	   WR_ZP(Y);  break;
		case 0x8C:/*ABS*/      WR_ABS(Y); break;
		case 0x94:/*ZP X*/	   WR_ZP_X(Y);  break;
			//Transfers
		case 0x8A:/*TXA*/ A = X; cycles=2; PC++; chk_neg_zero(A); break;		
		case 0x98:/*TYA*/ A = Y; cycles=2; PC++; chk_neg_zero(A); break;		
		case 0x9A:/*TXS*/ S = X; cycles=2; PC++; break;	
		case 0xA8:/*TAY*/ Y = A; cycles=2; PC++; chk_neg_zero(Y); break;
		case 0xAA:/*TAX*/ X = A; cycles=2; PC++; chk_neg_zero(X); break;
		case 0xBA:/*TSX*/ X = S; cycles=2; PC++; chk_neg_zero(X); break;	
		default:
		cout << "Unimplmented opcode 0x" << hex <<  (int)op << endl;
		is_running = false;
		throw 0;
		//abort();
		break;
	}
	//	if(PC!=0x8057){ 
 // 	printp();
   // printf(" A:%.2x, X:%.2x, Y:%.2x, S:%.2x\n", A, X, Y, S);}


total_cycles += cycles *3;
if(total_cycles >= 341) total_cycles -= 341;
}
void B6502::push_stack()
{
	//stack operations should still be done by
	// direct access, since it's a hardwired address
	p_RAM[0][0x100+S--] = PC >> 8;
	p_RAM[0][0x100+S--] = PC & 0xFF;
	p_RAM[0][0x100+S--] = P;
	//write_ram(0x100+S--, PC >> 8);
	//write_ram(0x100+S--, PC & 0xFF);
	//write_ram(0x100+S--, P);
}
void B6502::irq()
{
	push_stack();
	PC = read_ram(0xfffe)+ (read_ram(0xffff)<<8);
	IRQ = false;
}
void B6502::nmi()
{
	push_stack();
	PC = read_ram(0xfffa)+ (read_ram(0xfffb)<<8);
	NMI = false;
}
void B6502::logger(uint8_t op)
{
	switch(op)
	 {
			//Break
		case 0x0: 
	//	cout << "BRK!" << endl;
	//	cout << "Halting CPU" << endl;
	//	is_running = false;
	//	throw 0;
		break;
			//ADC
		case 0x65:/*ZP*/  
		case 0x69:/*IMM*/ 
		case 0x6D:/*ABS*/  
		case 0x79:/*ABS Y*/  cout << " ADC "; break;
			//AND
		case 0x29:/*IMM*/	  
		case 0x2D:/*ABS*/     	
		case 0x3D:/*ABS_X*/  cout << " AND "; break;	
			//ASL
		case 0x0A:/*ACC*/	 cout << " ASL "; break;
			//Branches
		case 0x10:/*BPL*/ 	 cout << " BPL ";break;		
		case 0x30:/*BMI*/   cout << " BMI ";	break;	
		case 0x50:/*BVC*/	 cout << " BVC ";break;		
		case 0x70:/*BVS*/ cout << " BVS ";break;	
		case 0x90:/*BCC*/ 	 cout << " BCC ";break;
		case 0xB0:/*BCS*/ 	 cout << " BCS ";break;
		case 0xD0:/*BNE*/  cout << " BNE ";break;
		case 0xF0:/*BEQ*/ 	 cout << " BEQ ";break;
			//BIT
		case 0x24:/*ZP */ 
		case 0x2C:/*ABSOLUTE*/ cout << " BIT ";break; 
			//CMP
		case 0xC9:/*IMM*/ 
		case 0xD9:/*ABS Y*/  cout << " CMP ";break;
			//CPX
		case 0xE0:/*IMM*/ cout << " CPX "; break;
			//CPY
		case 0xC0:/*IMM*/  break;
			//DEC
		case 0xC6:/*ZP*/break;
		case 0xCE:/*ABS*/ break;
		
			//DEX
		case 0xCA: 	break;
			//DEY
		case 0x88: break;		
			//EOR
		case 0x45:/*ZP*/	    break;
		case 0x49:/*IMM*/	  break;
			//Flags
        case 0x18: 			 break;
        case 0x38: 		break;
        case 0x58: 			break;
        case 0x78: 		break;
        case 0xB8:		break;
        case 0xD8: 			break;
        case 0xF8: 			 break;
			//INC
		case 0xE6:/*ZP*/	   				break;
		case 0xEE:/*ABS*/  				break;
			//INY
		case 0xC8:        break;
			//INX
		case 0xE8:         break;
			//Jumps
		case 0x20:/*JSR*/   break;
		case 0x40:/*RTI*/   break;
		case 0x60:/*RTS*/      break;
		case 0x4C:/*JMP ABS*/   		break;
		case 0x6C:/*JMP IND*/  						break;
			//LDA
		case 0xA5:/*ZP*/	   break;
		case 0xB5:/*ZP_X*/	    break;
		case 0xA9:/*IMM*/	    break;		
		case 0xAD:/*ABSOLUTE*/ break;	
		case 0xB1:/*IND_Y*/     break;
		case 0xA1:/*IND_X*/     break;
		case 0xB9:/*ABS_Y*/    break;
		case 0xBD:/*ABS_X*/    break;
			//LDX
		case 0xA2:/*IMM*/ 	   break;
		case 0xA6:/*ZP*/	  break;
		case 0xAE:/*ABSOLUTE*/break;
		case 0xBE:/*ABS_Y*/     break;
			//LDY
		case 0xA0:/*IMM*/ 	    break; 
		case 0xA4:/*ZP*/	   break;
		case 0xAC:/*ABSOLUTE*/ break;
			//LSR
		case 0x4A:/*ACC*/	  	break;
			//NOPs
		case 0xEA: break;
			//ORA
		case 0x05:/*ZP*/	 break;
		case 0x09:/*IMM*/	    break;
		case 0x01:/*IND_X*/    break;	
			//Pushes and Pulls
		case 0x48:/*PHA*/     break;
		case 0x68:/*PLA*/     break;
			//ROL
	//	case 0x26:/*ZP*/	    break;
		case 0x2A:/*ACC*/	  	break;
			//ROR
		case 0x6A:/*ACC*/	 break;
		case 0x7E:/*ABS X*/	 	break;
			//SBC
		case 0xF9:/*ABS Y*/	 			break;	
		case 0xE9:/*IMM*/ break;
			//STA
		case 0x85:/*ZP*/     break;
		case 0x8D:/*ABSOLUTE*/  break;
		case 0x91:/*IND Y*/   break;
		case 0x99:/*ABS Y*/   break;
		case 0x9D:/*ABS X*/   break;
			//STX	
		case 0x86:/*ZP*/ 	   break;
		case 0x8E:/*ABS*/	   break;
			//STY
		case 0x84:/*ZP*/	  break;
		case 0x8C:/*ABS*/     break;
			//Transfers
		case 0x8A:/*TXA*/ break;		
		case 0x98:/*TYA*/  break;		
		case 0x9A:/*TXS*/break;	
		case 0xA8:/*TAY*/  break;
		case 0xAA:/*TAX*/  break;
		case 0xBA:/*TSX*/break;	
		default:
		cout << "Unimplmented opcode 0x" << hex <<  (int)op << endl;
			break;
	}
}
