#include "input.h"
#include <assert.h>
#include <iostream>
using namespace std;
INPUT::INPUT(APU* _apu, PPU* _ppu)
{
	assert(_apu != NULL);
	apu = _apu;	
	assert(_ppu != NULL);
	ppu = _ppu;

}

/*
void INPUT::pad_write(uint8_t val)
{
	if(val & 0x1)
		strobe = true;
	if(strobe && !(val & 0x1))
		pad_enabled = true;

}



*/

uint8_t INPUT::read_io(uint16_t addr)
{

//	cout << "IO read @ Address: $" << hex << (int)addr << endl;
	switch(addr)
	{
		case 0x4016:
//		cout << "Reading buttons on pad" << endl;
		SDL_PollEvent(&event);
    	if(event.type==SDL_QUIT) exit(0);
		return 0x00;
		break;
		case 0x4017:
		return 0x00;
		default:
//		cout << "Invalid address passed to read_io!" << endl;
		break;

	}
}

void INPUT::write_io(uint16_t addr, uint8_t value)
{

//	cout << "IO write @ Address: $" << (int)addr << " Value: " << (int)value << endl;
	switch(addr)
	{
		case 0x4011: case 0x4015:
		apu->write_apu(addr,value);
		break;
		case 0x4016:
//			cout << "Writing to $4016, trying to set clock for pads" << endl;
			if(value & 0x1)
				pad_strobe = true;
			else if(pad_strobe)
			{
				pad_enabled = true;
				pad_strobe = false;
			}
		break;
		default:
	//	cout << "Invalid address passed to write_io!" << endl;
		break;

	}

}
