#include "nes.h"
#include <iostream>
#include <fstream>
#include <assert.h>
using namespace std;



uint8_t B6502::read_ram(uint16_t addr)
{
	uint16_t upper_bank = addr >> 13;
	
	switch (upper_bank)
	{
		case 0x0: 
			return p_RAM[0][addr & 0x7FF];
	    case 0x1: 
			return ((class NES*)emulator)->ppu->read_ppu(addr);
		case 0x2:
			 return ((class NES*)emulator)->input->read_io(addr);
		case 0x3:
			cout << "Unimplemented SRAM read @ bank: " \
				<< hex << upper_bank << endl;
			abort();
		break;
		case 0x4: case 0x5: case 0x6: case 0x7:	
			return p_RAM[upper_bank][addr & 0x1FFF];
	}
	cout << "Shouldn't be here! (read_ram)" << endl;
	abort();
}
void B6502::write_ram(uint16_t addr, uint8_t value)
{
	uint16_t upper_bank = addr >> 13;
	switch (upper_bank)
	{
		case 0:
			p_RAM[0][addr & 0x7FF] = value;
		return;
	    
		case 0x1: 
			((class NES*)emulator)->ppu->write_ppu(addr, value);
		return;
		
		case 0x2:
			((class NES *)emulator)->input->write_io(addr,value);
		return;
	
		case 0x3:
			cout << "Unimplemented SRAM write @ bank: " << hex << upper_bank << endl;
			abort();
		return;
		
		case 0x4: case 0x5: case 0x6: case 0x7:	
			p_RAM[upper_bank][addr & 0x1FFF] = value;
		return;

	}
	cout << "Shouldn't be here! (write_ram)" << endl;
	abort();
}

NES::NES(char *rom)
{
	try{ 
    	cart = new Cart(rom);	//throw exception if file not found
	}
	catch (char *e){
		throw e;//since we can't do anything, throw error out
	}
	
	cpu = new B6502();
	ppu = new PPU();
	apu = new APU();
	input = new INPUT(apu, ppu);
	//allows access to NES members within B6502's scope
	cpu->emulator = (class EMULATOR *)this;
	ppu->nes = this;
	//ppu->nes = this;
	//set RAM to 0xFF
	memset(&RAM, 0x00, 0x800);
	//sets first p_RAM to RAM
	cpu->p_RAM[0] = &RAM[0];
	
    //sets iniial cartridge mirroring
	//which can be changed latter once mappers are implemented	
	for(int i = 4; i < 0x8; i++)
		cpu->p_RAM[i] = cart->prg_banks[i - 4];
	//need to setup CHR pointer in ppu
	//for(int i = 0; i < 4; i++)
	//	ppu->p_VRAM[i] = cart->chr_banks[i];
	// or
	for(int i = 0; i < 8; i++)
		memcpy(&ppu->VRAM[0x400*i], cart->chr_banks[i], 0x400);

	//this sets PC to the startup vector
	cpu->init_cpu();
	//prerenders pattern table
	ppu->chr_pre_render();	
	
}

void NES::dump_ram()
{

	ofstream ram_dump("ram.dmp", ios::out | ios::binary);
	if(!ram_dump.is_open()) abort();
	ram_dump.write((char *)&RAM, 0x800);
	ram_dump.close();

}

void NES::run_nes()
{
	cpu->is_running = true;
	register int cycles = 0;
	while(cpu->is_running)
	{
		while(cycles < 341*5*256)
		{
			
			if(cpu->NMI)
			{
				cpu->nmi();
				ppu->ppu_ctrl |= 0x80;
			}
			//remove try/catch once cpu is done
			try{cpu->do_op();}
			catch(int e){return;}

			ppu->cycles_to_run += cpu->get_cycles();
			apu->cycles_to_run += cpu->get_cycles();
			cycles 			   += cpu->get_cycles();
		}
		ppu->run_ppu();
		//apu->run_apu()
		cycles = 0;
		cout << ppu->frames << endl;
		if(ppu->frames >= ppu->frames_to_run) return;
	}
}
