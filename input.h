#include <stdint.h>
#include "apu.h"
#include "ppu.h"
#include <SDL/SDL.h>
class INPUT {
	public:
	INPUT(APU* _apu, PPU* _ppu);
	void write_io(uint16_t addr, uint8_t value);
	uint8_t read_io(uint16_t addr);	
	struct {} input_config;
	private:
	APU *apu;
	PPU *ppu;
	SDL_Event event;
	bool pad_strobe;
	bool pad_enabled;

};
