#++NES Makefile

CC=g++
CFLAGS=-c -arch i386 -Wall $(shell sdl-config --cflags) \
	$(shell wx-config --cppflags) -g -s -finline-functions \
	#-Wall -fexpensive-optimizations -O3
LDFLAGS= -arch i386 -framework Foundation $(shell wx-config --libs)\
	$(shell sdl-config --libs)
ODIR=obj

SOURCES=main.cpp apu.cpp B6502.cpp cartridge.cpp input.cpp nes.cpp ppu.cpp
HEADERS=main.h apu.h B6502.h cartridge.h input.h nes.h ppu.h
OBJECTS=$(SOURCES:.cpp=.o) 
LIBRARIES=#libSOIL.a
OBJS=$(patsubst %,$(ODIR)/%,$(OBJECTS))
EXECUTABLE=BNES++

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJS) $(HEADERS) 
	$(CC) $(LDFLAGS) $(OBJS) $(LIBRARIES) -o $@

$(ODIR)/%.o: %.cpp $(HEADERS)
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm obj/*.o $(EXECUTABLE)

test: all
	./$(EXECUTABLE) roms/smario.nes 200
