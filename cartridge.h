#include <stdint.h>
class Cart {
	public: 
		Cart (char *filename);
		~Cart();
		void print(void);
		int prg_size() const 
			{return header.PRG_banks*0x4000;};
		int chr_size() const
			{return header.CHR_banks*0x2000;};
		uint8_t mapper() const
			{return (header.ROM_info >> 4) + (header.ROM_info2 & 0xf0);};
		enum t_mirror {MIRROR_HORIZONTAL, MIRROR_VERTICAL, MIRROR_FOUR_SCREEN} ;
		t_mirror  mirroring() const
			{if(header.ROM_info& 0x1) return MIRROR_VERTICAL;
			else if(header.ROM_info & 0x8) return MIRROR_FOUR_SCREEN;
			else return MIRROR_HORIZONTAL;};
		bool trainer() const;
		bool region() const
			{return (bool)!(header.Region & 0x01);};
		bool battery() const
			{return (bool)(header.ROM_info & 0x02);};
		uint8_t *prg;
		uint8_t *prg_banks[64];
		uint8_t *chr;
		uint8_t *chr_banks[64];	
		uint8_t *battery_save;
	private:
		struct {
			char NES[4];
        	char PRG_banks;
        	char CHR_banks;
  	    	char ROM_info;
  	    	char ROM_info2;
			char Region;
			char INES_reserved[7];
		} header;
		bool loaded;
		uint16_t PRG_size;
		uint16_t CHR_size;	
};

